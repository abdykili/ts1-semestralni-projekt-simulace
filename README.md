## Task manager

### Application description

This desktop application is simple task manager. Task manager is used for organizing our work and work of our collegues or employees. Managing tasks is important especially when there is a lot of them. For each task we can set a target date or even exact time, which means we can simply handle our deadlines. Other very important usage is managing people working on our tasks. With task manager we can assign each task to one or more persons. Task manager also help us see how bussy our coworkers are. When new task appear we can compare coworkers timesheets and than assing the task to the least bussy one. Everytime someone completes their task, they can easilly checkbox the task so that manager could see the task is done. Every task can have its own subtasks. This is useful when there is one complex task which we have to divide in order to be completed. Program allows to export list of all tasks. 


### Funcionality overview
#### Tasks
- create
- edit
    - edit title
    - asign
    - specify priority
    - mark as completed
    - create subtasks
- delete
- assign to user

#### Tasks filtering
- filter by status (all, active, completed)
- filter by priority (all, high, medium, low)
- filter by due date (today, this week, this month, next month)
- filter by asignee (all, user)
- search by title

#### Preferences
- edit background color

#### Sorting
- sort by asignee ascending and descending
- sort by title ascending and descending
- sort by priority ascending and descending
- sort by due date ascending and descending

### Application parts overview

Application is desingend with MVC architecture pattern. The main parts are model, view and controller.
![Screenshot](model1.png)
![Screenshot](model2.png)
![Screenshot](model3.png)
#### View
Swing framework is used for application frontend. All of components are based on Swing:
- JFrame
- JPannel
- JLabel
- JDialog
- JCheckBox
- JTable
- JButton
- JPopupMenu

#### Controller
The main router is designed in AppController class. When event is fired by components, controller handles the action.

#### Model
Current state of application, all tasks and user is handled by TaskListManager. Main models representing entities are:
- User
- Task





